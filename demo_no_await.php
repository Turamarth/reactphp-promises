<?php
echo '<br>Inici '. date('G:i:s') . "<br>";

require('vendor/autoload.php');


use React\Http\Browser;
use Psr\Http\Message\ResponseInterface;
use React\Async;

$url1 = 'http://localhost/promise3/sleep.php';
$url2 = 'http://localhost/promise3/sleep2.php';

$client = new React\Http\Browser();

$client->get($url1)->then(
    function (Psr\Http\Message\ResponseInterface $response) {
        // var_dump($response->getHeaders(), (string)$response->getBody()); // EXAMPLE GET
        $code = $response->getStatusCode();
        echo '<br> Sleep 1 '. date('G:i:s');
    },
    function (Exception $e) {
        echo 'Error: ' . $e->getMessage() . PHP_EOL;
    }
);

$client->get($url2)->then(
    function (Psr\Http\Message\ResponseInterface $response) {
        $code = $response->getStatusCode();
        echo '<br> Sleep 2 '. date('G:i:s');
    },
    function (Exception $e) {
        echo 'Error: ' . $e->getMessage() . PHP_EOL;
    }
);


echo '<br> Fi '. date('G:i:s') . "<br>";