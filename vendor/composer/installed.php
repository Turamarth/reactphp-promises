<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'evenement/evenement' => array(
            'pretty_version' => 'v3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../evenement/evenement',
            'aliases' => array(),
            'reference' => '531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
            'dev_requirement' => false,
        ),
        'fig/http-message-util' => array(
            'pretty_version' => '1.1.5',
            'version' => '1.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../fig/http-message-util',
            'aliases' => array(),
            'reference' => '9d94dc0154230ac39e5bf89398b324a86f63f765',
            'dev_requirement' => false,
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'react/async' => array(
            'pretty_version' => 'v4.0.0',
            'version' => '4.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/async',
            'aliases' => array(),
            'reference' => '2aa8d89057e1059f59666e4204100636249b7be0',
            'dev_requirement' => false,
        ),
        'react/cache' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/cache',
            'aliases' => array(),
            'reference' => 'd47c472b64aa5608225f47965a484b75c7817d5b',
            'dev_requirement' => false,
        ),
        'react/dns' => array(
            'pretty_version' => 'v1.10.0',
            'version' => '1.10.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/dns',
            'aliases' => array(),
            'reference' => 'a5427e7dfa47713e438016905605819d101f238c',
            'dev_requirement' => false,
        ),
        'react/event-loop' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/event-loop',
            'aliases' => array(),
            'reference' => '187fb56f46d424afb6ec4ad089269c72eec2e137',
            'dev_requirement' => false,
        ),
        'react/http' => array(
            'pretty_version' => 'v1.8.0',
            'version' => '1.8.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/http',
            'aliases' => array(),
            'reference' => 'aa7512ee17258c88466de30f9cb44ec5f9df3ff3',
            'dev_requirement' => false,
        ),
        'react/promise' => array(
            'pretty_version' => 'v2.9.0',
            'version' => '2.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/promise',
            'aliases' => array(),
            'reference' => '234f8fd1023c9158e2314fa9d7d0e6a83db42910',
            'dev_requirement' => false,
        ),
        'react/promise-stream' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/promise-stream',
            'aliases' => array(),
            'reference' => 'e6d2805e09ad50c4896f65f5e8705fe4ee7731a3',
            'dev_requirement' => false,
        ),
        'react/promise-timer' => array(
            'pretty_version' => 'v1.9.0',
            'version' => '1.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/promise-timer',
            'aliases' => array(),
            'reference' => 'aa7a73c74b8d8c0f622f5982ff7b0351bc29e495',
            'dev_requirement' => false,
        ),
        'react/socket' => array(
            'pretty_version' => 'v1.12.0',
            'version' => '1.12.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/socket',
            'aliases' => array(),
            'reference' => '81e1b4d7f5450ebd8d2e9a95bb008bb15ca95a7b',
            'dev_requirement' => false,
        ),
        'react/stream' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../react/stream',
            'aliases' => array(),
            'reference' => '7a423506ee1903e89f1e08ec5f0ed430ff784ae9',
            'dev_requirement' => false,
        ),
        'ringcentral/psr7' => array(
            'pretty_version' => '1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ringcentral/psr7',
            'aliases' => array(),
            'reference' => '360faaec4b563958b673fb52bbe94e37f14bc686',
            'dev_requirement' => false,
        ),
    ),
);
