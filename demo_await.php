<?php
echo '<br>Inici '. date('G:i:s') . "<br>";

require('vendor/autoload.php');

use React\Http\Browser;
use Psr\Http\Message\ResponseInterface;
use React\Async;
use function React\Async\await;
use function React\Promise\all;

$url1 = 'http://localhost/promise3/sleep.php'; // URL
$url2 = 'http://localhost/promise3/sleep2.php'; // URL

$browser = new React\Http\Browser();

$promises = array(
    $browser->get($url1)->then(function (Psr\Http\Message\ResponseInterface $response) {
        echo '<br> Sleep 1 '. date('G:i:s');
    }, function (Exception $e) {
        echo 'Error: ' . $e->getMessage() . PHP_EOL;
    }),
    $browser->get($url2)->then(function (Psr\Http\Message\ResponseInterface $response) {
        echo '<br> Sleep 2 '. date('G:i:s');
    }, function (Exception $e) {
        echo 'Error: ' . $e->getMessage() . PHP_EOL;
    }),
);

$responses = await(all($promises));

echo '<br><br> Fi '. date('G:i:s') . "<br>";